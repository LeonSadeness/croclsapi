﻿using CrocServer.DataLayers;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CrocServer.Game
{
    public class GameCrocService
    {
        readonly ICrocDataLayer _crocDataLayer;

        public GameCrocService(ICrocDataLayer crocDataLayer)
        {
            _crocDataLayer = crocDataLayer;
        }
    }
}