using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CrocServer.AuthHandlers;
using CrocServer.AuthorizeHandlers;
using CrocServer.DataLayers;
using CrocServer.Game;
using CrocServer.Hubs;
using CrocServer.Tools;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

namespace CrocServer
{
    public class Startup
    {
        public const string CookieScheme = "CrocCookie";

        public IConfiguration Configuration { get; }


        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            string urlOrigin = Configuration.GetValue<string>("urlOrigin", "ur_not_found");

            services.AddCors(options => options.AddPolicy("ProdPolicy",
                builder =>
                    {
                        builder.AllowAnyMethod()
                        .AllowAnyHeader()
                        .AllowCredentials()
                        .WithOrigins(urlOrigin);
                    }));

            services.AddCors(options => options.AddPolicy("LocalPolicy",
                builder =>
                    {
                        builder.AllowAnyMethod()
                        .AllowAnyHeader()
                        .AllowCredentials()
                        .WithOrigins("http://localhost:8080");
                    }));

            //services.AddHttpsRedirection(options => { options.HttpsPort = 443; });

            #region CookieHandlerAuth

            //services.AddSingleton<IConfigureOptions<CookieAuthenticationOptions>, ConfigureMyCookie>();
            //services.AddAuthentication(CookieScheme).AddCookie(CookieScheme); 

            //services.AddSingleton<IAuthorizationHandler, OnGameAuthorizeHandler>();
            //services.AddAuthorization(options =>
            //{
            //    options.AddPolicy("OnGame", policy =>
            //    {
            //        policy.Requirements.Add(new OnGameRequirement());
            //    });
            //});

            #endregion

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                {
                    options.RequireHttpsMetadata = false;
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuer = true,
                        ValidIssuer = AuthJWTOptions.ISSUER,
                        ValidateAudience = true,
                        ValidAudience = AuthJWTOptions.AUDIENCE,
                        ValidateLifetime = true,
                        IssuerSigningKey = AuthJWTOptions.GetSymmetricSecurityKey(),
                        ValidateIssuerSigningKey = true,
                    };
                    options.Events = new JwtBearerEvents
                    {
                        OnMessageReceived = context =>
                        {
                            var accessToken = context.Request.Query["access_token"];

                            // ���� ������ ��������� ����
                            var path = context.HttpContext.Request.Path;
                            if (!string.IsNullOrEmpty(accessToken) &&
                                (path.StartsWithSegments("/game")))
                            {
                                // �������� ����� �� ������ �������
                                context.Token = accessToken;
                            }
                            return Task.CompletedTask;
                        }
                    };
                });

            services.AddSingleton<ICrocDataLayer>(new CrocDataLayer());
            services.AddSignalR();
            services.AddControllers();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            var policy = env.IsProduction() ? "ProdPolicy" : "LocalPolicy";

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHttpsRedirection();
            }

            app.UseDefaultFiles();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseCors(policy);
            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapHub<RoomHub>("/game");
            });
        }
    }
}
