﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.SignalR;
using Microsoft.AspNetCore.Authentication;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using System.Linq;
using CrocServer.DataLayers;

namespace CrocServer.Hubs
{
    [Authorize]
    public class RoomHub : Hub
    {
        readonly ICrocDataLayer _crocDataLayer;

        public RoomHub(ICrocDataLayer crocDataLayer)
        {
            _crocDataLayer = crocDataLayer;
        }

        public override async Task OnConnectedAsync()
        {
            var user = _crocDataLayer.GetUser(Context.User.Identity.Name);
            if (user == null)
                await SignOutUserAsync();
            else
            {
                var room = _crocDataLayer.GetRoomFromUser(user);
                if (room == null)
                {
                    await SignOutUserAsync();
                }
                else
                {
                    _crocDataLayer.AddUserInRoom(user, room);
                    await Groups.AddToGroupAsync(Context.ConnectionId, user.Room.NameRoom);
                    await UsersList();
                }
            }

            await base.OnConnectedAsync();
        }

        private async Task SignOutUserAsync()
        {
            //await Context.GetHttpContext().SignOutAsync(JwtBearerDefaults.AuthenticationScheme);

            await SignOut();
            await Task.Run(() => _crocDataLayer.DeleteUser(Context.User.Identity.Name));

            Context.Abort();
        }

        public override async Task OnDisconnectedAsync(Exception exception)
        {
            _crocDataLayer.LeaveUserFromRoom(Context.User.Identity.Name);
            await UsersList();

            await base.OnDisconnectedAsync(exception);
        }

        #region ClientMethods

        public async Task NewMsg(object data)
        {
            var room = _crocDataLayer.GetRoomFromUser(Context.User.Identity.Name);
            if (room == null)
                await SignOutUserAsync();
            else
            {

                await this.Clients.OthersInGroup(room.NameRoom).SendAsync("NewMsg", data);
            }
        }

        public async Task NewDraw(string data)
        {
            var room = _crocDataLayer.GetRoomFromUser(Context.User.Identity.Name);
            if (room == null)
                await SignOutUserAsync();
            else
                await this.Clients.OthersInGroup(room.NameRoom).SendAsync("NewDraw", data);
        }

        public async Task OldDraw(string data)
        {
            var room = _crocDataLayer.GetRoomFromUser(Context.User.Identity.Name);
            if (room == null)
                await SignOutUserAsync();
            else
                await this.Clients.OthersInGroup(room.NameRoom).SendAsync("OldDraw", data);
        }

        private async Task UsersList()
        {
            var room = _crocDataLayer.GetRoomFromUser(Context.User.Identity.Name);
            if (room != null)
            {
                var users = JsonConvert.SerializeObject(room.Users);
                await this.Clients.Group(room.NameRoom).SendAsync("UsersList", users);
            }
        }

        /// <summary>
        /// Говорит, клиенту что он был отклеч и должен удалить токен
        /// </summary>
        /// <returns></returns>
        private async Task SignOut()
        {
            await this.Clients.Caller.SendAsync("SignOut");
        }

        #endregion
    }
}