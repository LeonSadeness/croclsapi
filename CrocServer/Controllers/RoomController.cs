﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using CrocServer.DataLayers;
using CrocServer.Models;
using CrocServer.Tools;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;

namespace CrocServer.Controllers
{
    [Route("[controller]")]
    public class RoomController : ControllerBase
    {
        readonly ICrocDataLayer _crocDataLayer;

        public RoomController(ICrocDataLayer crocDataLayer)
        {
            _crocDataLayer = crocDataLayer;
        }

        [HttpPost("join")]
        public IActionResult JoinRoom([FromBody]UserRequestData data)
        {
            if (!ModelState.IsValid) return BadRequest();
            if (string.IsNullOrWhiteSpace(data.NameRoom)) BadRequest();

            var room = _crocDataLayer.Rooms.FirstOrDefault(r => r.NameRoom == data.NameRoom && (r.PassRoom == data.PassRoom || string.IsNullOrEmpty(r.PassRoom)));
            if (room == null) return Forbid();

            var identity = GetIdentity(data, true);

            var now = DateTime.UtcNow;
            // создаем JWT-токен
            var jwt = new JwtSecurityToken(
                    issuer: AuthJWTOptions.ISSUER,
                    audience: AuthJWTOptions.AUDIENCE,
                    notBefore: now,
                    claims: identity.Claims,
                    expires: now.Add(TimeSpan.FromMinutes(AuthJWTOptions.LIFETIME)),
                    signingCredentials: new SigningCredentials(AuthJWTOptions.GetSymmetricSecurityKey(), SecurityAlgorithms.HmacSha256));
            var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);

            var response = new
            {
                access_token = encodedJwt,
                username = identity.Name
            };
            return Ok(response);
        }

        [HttpPost("create")]
        public IActionResult CreateRoom([FromBody]UserRequestData data)
        {
            if (!ModelState.IsValid) return BadRequest();
            var identity = GetIdentity(data);

            var now = DateTime.UtcNow;
            // создаем JWT-токен
            var jwt = new JwtSecurityToken(
                    issuer: AuthJWTOptions.ISSUER,
                    audience: AuthJWTOptions.AUDIENCE,
                    notBefore: now,
                    claims: identity.Claims,
                    expires: now.Add(TimeSpan.FromMinutes(AuthJWTOptions.LIFETIME)),
                    signingCredentials: new SigningCredentials(AuthJWTOptions.GetSymmetricSecurityKey(), SecurityAlgorithms.HmacSha256));
            var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);

            var response = new
            {
                access_token = encodedJwt,
                username = identity.Name
            };
            return Ok(response);
        }

        private ClaimsIdentity GetIdentity(UserRequestData data, bool join = false)
        {
            string guid;
            do guid = Guid.NewGuid().ToString();
            while (_crocDataLayer.Users.Any(u => u.Id == guid));

            Room room;

            if (join)
            {
                room = _crocDataLayer.GetRoom(data.NameRoom);
            }
            else
            {
                room = new Room
                {
                    NameRoom = data.NameRoom,
                    PassRoom = data.PassRoom,
                };
                room.Users = new List<UserCroc>();
                _crocDataLayer.Rooms.Add(room);
            }

            var user = new UserCroc
            {
                Id = guid,
                UserName = data.Name,
                AvatarId = data.Avatar,
                Room = room,
                Scores = 0
            };

            _crocDataLayer.Users.Add(user);
            if(!join) room.RoomCreator = user;

            var claims = new List<Claim>
                {
                    new Claim(ClaimTypes.Name, guid),
                };
            var claimsIdentity = new ClaimsIdentity(claims, "Token");

            return claimsIdentity;
        }

        [HttpGet("list")]
        public IActionResult GetAllRooms()
        {
            //{ name: "name_room_text", pass: true, creator: "ID_user" count: 2 }
            var users = _crocDataLayer.Rooms.Select(r => new
            {
                name = r.NameRoom,
                pass = !string.IsNullOrEmpty(r.PassRoom),
                creator = r.RoomCreator.Id,
                count = r.Users.Count
            });
            return Ok(JsonConvert.SerializeObject(users));
        }

        [HttpPost("check/pass")]
        public IActionResult CheckPassRoom([FromBody]UserRequestData data)
        {
            if (!ModelState.IsValid) return BadRequest();
            var room = _crocDataLayer.Rooms.FirstOrDefault(r => r.NameRoom == data.NameRoom && r.PassRoom == data.PassRoom);

            if (room == null)
                return Forbid();

            return Ok();
        }


        [HttpPost("check/name")]
        public IActionResult CheckNameRoom([FromBody]UserRequestData data)
        {
            if (!ModelState.IsValid) return BadRequest();
            var finded = _crocDataLayer.Rooms.Any(r => r.NameRoom == data.NameRoom);

            if (finded)
                return BadRequest();

            return Ok();
        }

        #region Resive

        //[HttpPost]
        //[Route("create")]
        //public IActionResult CreateRoom([FromBody]UserRequestData data)
        //{
        //    var h = HttpContext;

        //    if (!ModelState.IsValid) return BadRequest();

        //    string guid;
        //    do guid = Guid.NewGuid().ToString();
        //    while (_crocDataLayer.Users.Any(u => u.Id == guid));

        //    var room = new Room
        //    {
        //        RoomName = data.NameRoom,
        //        PasswordRoom = data.PassRoom,
        //    };
        //    var user = new UserCroc
        //    {
        //        Id = guid,
        //        UserName = data.Name,
        //        AvatarId = data.Avatar,
        //        Room = room,
        //        Score = 0
        //    };
        //    room.Users = new List<UserCroc>();
        //    room.RoomCreator = user;

        //    _crocDataLayer.Users.Add(user);
        //    _crocDataLayer.Rooms.Add(room);

        //    var claims = new List<Claim>
        //        {
        //            new Claim(ClaimTypes.Name, guid),
        //        };
        //    var claimsIdentity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);

        //    var authProperties = new AuthenticationProperties
        //    {
        //        // AllowRefresh = <bool>,
        //        // Обновление сеанса аутентификации должно быть разрешено.

        //        ExpiresUtc = DateTimeOffset.UtcNow.AddMinutes(10),
        //        // Время истечения билета аутентификации.
        //        // значение, установленное здесь, переопределяет параметр ExpireTimeSpan
        //        // CookieAuthenticationOptions установлен с помощью AddCookie.

        //        // IsPersistent = true,
        //        // Сохраняется ли сеанс аутентификации в
        //        // несколько запросов. При использовании с файлами cookie, элементы управления
        //        // является ли время жизни куки абсолютным (соответствует
        //        // время жизни билета аутентификации) или на основе сеанса.

        //        // IssuedUtc = <DateTimeOffset>,
        //        // Время, когда был выдан билет для аутентификации.

        //        // RedirectUri = <string>
        //        // Полный путь или абсолютный URI для использования в качестве http
        //        // перенаправить значение ответа.
        //    };
        //    return SignIn(new ClaimsPrincipal(claimsIdentity), authProperties, CookieAuthenticationDefaults.AuthenticationScheme);
        //}

        #endregion
    }
}