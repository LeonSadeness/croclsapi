﻿using CrocServer.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace CrocServer.DataLayers
{
    public class CrocDataLayer : ICrocDataLayer
    {
        /// <summary>
        /// Все Пользователи
        /// </summary>
        public List<UserCroc> Users { get; set; } = new List<UserCroc>();

        /// <summary>
        /// Все комнаты
        /// </summary>
        public List<Room> Rooms { get; set; } = new List<Room>();

        /// <summary>
        /// Все подключения
        /// </summary>
        public List<Connection> Connections { get; set; } = new List<Connection>();

        #region UsersMethod

        /// <summary>
        /// Ищет и возвращает пользователя по его ID
        /// </summary>
        /// <param name="userId">ID пользователя</param>
        /// <returns>Пользователь</returns>
        public UserCroc GetUser(string userId) => Users.FirstOrDefault(u => u.Id == userId);

        /// <summary>
        /// Удаляет пользователя по ID из комнаты но оставляет профиль
        /// </summary>
        /// <param name="userId">ID пользователя</param>
        public void LeaveUserFromRoom(string userId) => LeaveUserFromRoom(GetUser(userId));

        /// <summary>
        /// Удаляет пользователя из комнаты но оставляет профиль
        /// </summary>
        /// <param name="user">Пользователь</param>
        public void LeaveUserFromRoom(UserCroc user)
        {
            if (user != null)
            {
                var room = GetRoomFromUser(user);
                if (room != null)
                    if (room.Users.Where(u => u.Online).Count() < 2)
                        DeleteRoom(room);
                    else
                        user.Online = false;
            }
        }

        /// <summary>
        /// Удаляет пользователя по его ID
        /// </summary>
        /// <param name="userId">ID пользователя</param>
        public void DeleteUser(string userId) => DeleteUser(GetUser(userId));

        /// <summary>
        /// Удаляет пользователя
        /// </summary>
        /// <param name="user">Пользователь</param>
        public void DeleteUser(UserCroc user)
        {
            if (user != null)
                Users.Remove(user);

            var room = GetRoomFromUser(user);
            if (room != null)
                room.Users.Remove(user);
        }

        /// <summary>
        /// Добавляет очки пользователю по его ID
        /// </summary>
        /// <param name="userId">ID пользователя</param>
        /// <param name="score">Кол-во добавляемых очков</param>
        public void AddScoreUser(string userId, int score) => AddScoreUser(GetUser(userId), score);

        /// <summary>
        /// Добавляет очки пользователю по его ID
        /// </summary>
        /// <param name="user">Пользователь</param>
        /// <param name="score">Кол-во добавляемых очков</param>
        public void AddScoreUser(UserCroc user, int score)
        {
            if (user != null)
                user.Scores += score;
        }

        /// <summary>
        /// Обновляет очки пользователя по его ID
        /// </summary>
        /// <param name="userId">ID пользователя</param>
        /// <param name="score">Очки</param>
        public void UpdateScoreUser(string userId, int score) => UpdateScoreUser(GetUser(userId), score);

        /// <summary>
        /// Обновляет очки пользователя
        /// </summary>
        /// <param name="user">Пользователь</param>
        /// <param name="score">очки</param>
        public void UpdateScoreUser(UserCroc user, int score)
        {
            if (user != null)
                user.Scores = score;
        }

        #endregion

        #region RoomsMethod

        /// <summary>
        /// Добавляет пользователя в комнату, или если он был переводит в онлайн статус пользователя
        /// </summary>
        /// <param name="user">Пользователь</param>
        public void AddUserInRoom(UserCroc user, Room room)
        {
            if (!room.Users.Contains(user)) room.Users.Add(user);
            user.Online = true;
        }

        /// <summary>
        /// Проверяет пароль для указанной комнаты
        /// Если указанной комнаты не существует или
        /// если пароль неверный возвращает false
        /// </summary>
        /// <param name="RoomName">Имя комнаты</param>
        /// <param name="pass">Пароль</param>
        /// <returns>Успех проверки</returns>
        public bool ValidatePassRoom(string RoomName, string pass) => ValidatePassRoom(GetRoom(RoomName), pass);

        /// <summary>
        /// Проверяет пароль для указанной комнаты
        /// Если указанной комнаты не существует или
        /// если пароль неверный возвращает false
        /// </summary>
        /// <param name="room">Комната</param>
        /// <param name="pass">Пароль</param>
        /// <returns>Успех проверки</returns>
        public bool ValidatePassRoom(Room room, string pass) => room != null && room.PassRoom == pass;

        /// <summary>
        /// Ищет и возвращает комнату по её имени если такая есть
        /// Возвращает null если комната не найдена
        /// </summary>
        /// <param name="RoomName">Имя комнаты</param>
        /// <returns>Комната</returns>
        public Room GetRoom(string RoomName) => Rooms.FirstOrDefault(r => r.NameRoom == RoomName);

        /// <summary>
        /// Ищет зарегестрированную комнату из пользовательских данных
        /// Если такой комнаты нет, или такого пользователя не существует
        /// возвращает null
        /// </summary>
        /// <param name="userId">ID пользователя</param>
        /// <returns>Комната</returns>
        public Room GetRoomFromUser(string userId) => GetRoomFromUser(GetUser(userId));

        /// <summary>
        /// Ищет зарегестрированную комнату из пользовательских данных
        /// Если такой комнаты нет, или такого пользователя не существует
        /// возвращает null
        /// </summary>
        /// <param name="user">Пользователь</param>
        /// <returns>Комната</returns>
        public Room GetRoomFromUser(UserCroc user)
        {
            if (user != null)
                return Rooms.FirstOrDefault(r => r == user.Room);
            return null;
        }

        /// <summary>
        /// Удаляет комнату по её имени и всех пользователей в ней
        /// </summary>
        /// <param name="roomName">Имя комнаты</param>
        /// <returns>Возвращает список всех пользователей, которые находились в комнате на момент удаления</returns>
        public List<UserCroc> DeleteRoom(string roomName) => DeleteRoom(GetRoom(roomName));

        /// <summary>
        /// Удаляет комнату и всех пользователей в ней
        /// </summary>
        /// <param name="room">Комната</param>
        /// <returns>Возвращает список всех пользователей, которые находились в комнате на момент удаления</returns>
        public List<UserCroc> DeleteRoom(Room room)
        {
            if (room != null)
            {
                var users = room.Users;
                foreach (var item in room.Users)
                    Users.Remove(item);
                Rooms.Remove(room);
                return users;
            }
            else return new List<UserCroc>();
        }

        #endregion
    }
}