﻿using CrocServer.Models;
using System.Collections.Generic;

namespace CrocServer.DataLayers
{
    public interface ICrocDataLayer
    {
        List<Connection> Connections { get; set; }
        List<Room> Rooms { get; set; }
        List<UserCroc> Users { get; set; }

        void AddScoreUser(string userId, int score);
        void AddScoreUser(UserCroc user, int score);
        void AddUserInRoom(UserCroc user, Room room);
        List<UserCroc> DeleteRoom(Room room);
        List<UserCroc> DeleteRoom(string roomName);
        void DeleteUser(string userId);
        void DeleteUser(UserCroc user);
        Room GetRoom(string RoomName);
        Room GetRoomFromUser(string userId);
        Room GetRoomFromUser(UserCroc user);
        UserCroc GetUser(string userId);
        void LeaveUserFromRoom(string userId);
        void LeaveUserFromRoom(UserCroc user);
        void UpdateScoreUser(string userId, int score);
        void UpdateScoreUser(UserCroc user, int score);
        bool ValidatePassRoom(Room room, string pass);
        bool ValidatePassRoom(string RoomName, string pass);
    }
}