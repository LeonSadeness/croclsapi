﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace CrocServer.AuthorizeHandlers
{
    public class OnGameAuthorizeHandler : AuthorizationHandler<OnGameRequirement>
    {
        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, OnGameRequirement requirement)
        {
            //if (context.Resource is AuthorizationFilterContext mvcContext)
            //{
            //    // Examine MVC-specific things like routing data.
            //}

            //if ()
            //{
            //    //TODO: Use the following if targeting a version of
            //    //.NET Framework older than 4.6:
            //    //      return Task.FromResult(0);
            //    return Task.CompletedTask;
            //}

            //var dateOfBirth = Convert.ToDateTime(
            //    context.User.FindFirst(c => c.Type == ClaimTypes.DateOfBirth &&
            //                                c.Issuer == "http://contoso.com").Value);

            //int calculatedAge = DateTime.Today.Year - dateOfBirth.Year;
            //if (dateOfBirth > DateTime.Today.AddYears(-calculatedAge))
            //{
            //    calculatedAge--;
            //}

            //if (calculatedAge >= requirement.MinimumAge)
            //{
            //    context.Succeed(requirement);
            //}

            //TODO: Use the following if targeting a version of
            //.NET Framework older than 4.6:
            //      return Task.FromResult(0);
            return Task.CompletedTask;
        }
    }
}