﻿namespace CrocServer.Models
{
    public class Connection
    {
        /// <summary>
        /// Id  подключения в SingnalR
        /// </summary>
        public string ConnectionID { get; set; }

        /// <summary>
        /// User Agent подключения
        /// </summary>
        public string UserAgent { get; set; }

        /// <summary>
        /// Статус подключения
        /// </summary>
        public bool Connected { get; set; }
    }
}