﻿using System.ComponentModel.DataAnnotations;

namespace CrocServer.Models
{
    public class UserRequestData
    {
        [Required]
        public string Name { get; set; }

        [Required]
        public int Avatar { get; set; }

        public string NameRoom { get; set; }

        public string PassRoom { get; set; }
    }
}