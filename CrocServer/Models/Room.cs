﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace CrocServer.Models
{
    public class Room
    {
        /// <summary>
        /// Имя комнаты
        /// </summary>
        public string NameRoom { get; set; }

        /// <summary>
        /// Список пользователей комнаты
        /// </summary>
        public virtual List<UserCroc> Users { get; set; }

        /// <summary>
        /// Создатель комнаты
        /// </summary>
        public UserCroc RoomCreator { get; set; }

        /// <summary>
        /// Пароль комнаты
        /// </summary>
        [JsonIgnore]
        public string PassRoom { get; set; }
    }
}