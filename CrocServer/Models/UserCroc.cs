﻿using Newtonsoft.Json;

namespace CrocServer.Models
{
    public class UserCroc
    {
        /// <summary>
        /// Уникальный идентификатор пользователя
        /// </summary>
        [JsonProperty("id")]
        public string Id { get; set; }

        /// <summary>
        /// Имя пользователя
        /// </summary>
        [JsonProperty("name")]
        public string UserName { get; set; }

        /// <summary>
        /// ID аватрки пользователя
        /// </summary>
        [JsonProperty("avatar")]
        public int AvatarId { get; set; }

        /// <summary>
        /// Очки пользователя
        /// </summary>
        [JsonProperty("scores")]
        public int Scores { get; set; }

        /// <summary>
        /// Cnfnec gjkmpjdfntkz
        /// </summary>
        [JsonProperty("online")]
        public bool Online { get; set; }

        /// <summary>
        /// Комната пользователя
        /// </summary>
        [JsonIgnore]
        public Room Room { get; set; }
    }
}